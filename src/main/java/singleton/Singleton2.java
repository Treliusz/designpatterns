package singleton;

public class Singleton2 {
    public String name;
    public static final Singleton2 INSTANCE = new Singleton2();
    private Singleton2() {

    }
}
