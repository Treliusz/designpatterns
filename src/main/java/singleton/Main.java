package singleton;

public class Main {
    public static void main(String[] args) {
        Singleton1 s1 = Singleton1.getInstance();
        //Singleton1 s2 = Singleton1.getInstance();

        s1.name = "Pierwszy";
        //s2.name = "Drugi";

        System.out.println(s1.name);
       // System.out.println(s2.name);

        Singleton2 s3 = Singleton2.INSTANCE;
        Singleton2 s4 = Singleton2.INSTANCE;
        s3.name = "Trzeci";
        s4.name = "Czwarty";
        System.out.println(s3.name);
        System.out.println(s4.name);
    }
}
