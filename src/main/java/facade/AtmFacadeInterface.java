package facade;

public interface AtmFacadeInterface {

    boolean identification(long cardNumber, int pin);

    float checkAccountBalanceAtm(long cardNumber);

    String withdrawCash(int amount);
}
