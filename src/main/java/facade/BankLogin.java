package facade;

public class BankLogin {

    public boolean identification(long cardNumber, int pin) {
        if (cardNumber == 123456789 && pin == 1234) {
            return true;
        } else {
            return false;
        }
    }

    public void mobileIdentification() {}
    public void webIdentification(){}
}
