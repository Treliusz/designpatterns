package facade;

public class BankSystem {

    public float checkAccountBalance(long cardNumber) {
        if (cardNumber == 123456789) {
            return 3560.25f;
        } else {
            return 0.0f;
        }
    }

    public String withdrawCash(int amount) {
        return "Wypłacono " + amount + " zł";
    }

    public String activateCard(long cardNumber) {
        return "Karta nr " + cardNumber + " is active";
    }

    public void payment() {}

    public void  transfer() {}
}
