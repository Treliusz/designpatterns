package facade;

public class AtmFacade implements AtmFacadeInterface {

    private BankLogin bankLogin;
    private BankSystem bankSystem;

    public AtmFacade() {
        bankLogin = new BankLogin();
        bankSystem = new BankSystem();
    }

    public boolean identification(long cardNumber, int pin) {
        boolean correct = false;
        correct = bankLogin.identification(cardNumber, pin);
        return correct;
    }

    public float checkAccountBalanceAtm(long cardNumber) {

        return bankSystem.checkAccountBalance(cardNumber);
    }

    public String withdrawCash(int amount) {
        return bankSystem.withdrawCash(amount);
    }
}
