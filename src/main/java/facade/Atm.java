package facade;

import java.util.Scanner;

public class Atm {
    private AtmFacade atmFacade = new AtmFacade();
    private static Atm atm = new Atm();

    public static void main(String[] args) {
        atm.atmStart();
    }

    public void atmStart() {

        int pin = 0;
        boolean identification = false;

        System.out.println("Witaj w BANKOMACIE");
        System.out.println("Włóż karte i podaj PIN");
        Scanner scanner = new Scanner(System.in);
        pin = scanner.nextInt();

        System.out.println("Autoryzacja...\n");

        identification = atmFacade.identification(123456789, pin);
        if (identification) {
            atm.showMenu();
        } else {
            System.out.println("Autoryzacja nie powiodła się.\n Koniec");
            atm.exit();
        }

    }


    public void showMenu() {
        System.out.println("Menu:");
        System.out.println("1. Stan konta");
        System.out.println("2. Wypłata gotówki");
        System.out.println("3. Wyjście");

        Scanner scanner = new Scanner(System.in);
        int choice = scanner.nextInt();

        switch (choice) {
            case 1:
                atm.showAccountBalance(123456789);
                break;
            case 2:
                atm.withdrawCash(500);
                break;
            case 3:
                atm.exit();
                break;
                default:
                    atm.showMenu();
        }
    }

    public void showAccountBalance(long cardNumber) {
        System.out.println(atmFacade.checkAccountBalanceAtm(cardNumber));
        atm.showMenu();
    }

    public void withdrawCash(int amount) {
        System.out.println(atmFacade.withdrawCash(amount));
        System.out.println("Odbierz kartę");
        atm.exit();
    }

    public void exit() {
        System.exit(0);
    }
}
