package builder2;

public interface CarBuilder {

    void buildTires();

    void buildEngine();

    Car getCar();
}
