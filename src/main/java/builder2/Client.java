package builder2;

public class Client {
    public static void main(String[] args) {
        CarBuilder carBuilder = new MercedesBuilder();
        CarDirector carDirector = new CarDirector(carBuilder);
        carDirector.makeCar();
        Car car = carDirector.getCar();
        System.out.println(car);
    }
}
