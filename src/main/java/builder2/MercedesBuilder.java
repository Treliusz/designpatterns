package builder2;

public class MercedesBuilder implements CarBuilder {

    private Car car;

    public MercedesBuilder() {
        this.car = new Car();
    }

    public void buildTires() {
        Tires tire = new Tires();
        tire.setDurability(50);
        tire.setType("Nokian");
        car.setTires(tire);
    }

    public void buildEngine() {
        Engine engine = new Engine();
        engine.setType("V8");
        car.setEngine(engine);
    }

    public Car getCar() {
        return car;
    }
}
