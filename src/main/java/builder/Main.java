package builder;

public class Main {
    public static void main(String[] args) {
        Car car = new Car.Builder()
                .color("Red")
                .brand("Audi")
                .numbersOfDoors(5)
                .build();
        System.out.println(car.getBrand());
        System.out.println(car);
        Person person = new Person.Builder()
                .firstName("Adam")
                .lastName("Nowak")
                .age(25)
                .pesel("93021523773")
                .nationality("Polish")
                .build();
        System.out.println(person.getPesel());
        System.out.println(person);


    }
}
