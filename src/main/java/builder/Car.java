package builder;

public class Car {

    private String color;
    private String brand;
    private int numberOfDoors;

    private Car(Builder builder) {
        this.color = builder.color;
        this.brand = builder.brand;
        this.numberOfDoors = builder.numberOfDoors;

    }

    public static class Builder {
        private String color;
        private String brand;
        private int numberOfDoors;


        public Builder color(String color) {
            this.color = color;
            return this;
        }

        public Builder brand(String brand) {
            this.brand = brand;
            return this;
        }

        public Builder numbersOfDoors(int numberOfDoors) {
            this.numberOfDoors = numberOfDoors;
            return this;
        }

        public Car build() {
            return new Car(this);
        }
    }
    public String getColor() {
        return color;
    }

    public String getBrand() {
        return brand;
    }

    public int getNumberOfDoors() {
        return numberOfDoors;
    }

    @Override
    public String toString() {
        return "Car{" +
                "color='" + color + '\'' +
                ", brand='" + brand + '\'' +
                ", numberOfDoors=" + numberOfDoors +
                '}';
    }
}
