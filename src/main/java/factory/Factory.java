package factory;

import java.util.Scanner;

public class Factory {
    public static void main(String[] args) {

        Factory factory = new Factory();

        System.out.println("Podaj markę samochodu:");
        Scanner scanner = new Scanner(System.in);
        String userBrand = scanner.nextLine();

        System.out.println(factory.getCar(userBrand).getBrand());
    }

    public Car getCar(String brand) {
        Car car;
        if (brand.equalsIgnoreCase("Citroen")) {
            car = new Citroen();
        } else if (brand.equalsIgnoreCase("Nissan")) {
            car = new Nissan();
        } else if (brand.equalsIgnoreCase("Toyota")) {
            car = new Toyota();
        } else {
            car = new Car();
        }

        return car;
    }
}
