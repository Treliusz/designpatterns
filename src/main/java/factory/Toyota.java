package factory;

public class Toyota extends Car {

    private String brand = "Toyota";

    @Override
    public String getBrand() {
        return brand;
    }
}
