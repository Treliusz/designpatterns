package factory;

public class Nissan extends Car {
    private String brand = "Nissan";

    @Override
    public String getBrand() {
        return brand;
    }
}
