package factory;

public class Citroen extends Car {

    private String brand = "Citroen";


    @Override
    public String getBrand() {
        return brand;
    }
}
