package observer;

public class Client {

    public static void main(String[] args) {

        Observer mario = new YTUser("Mario");
        Subject radioOne = new YTChannel();
        Observer greta = new YTUser("Greta");

        radioOne.register(mario);
        ((YTChannel) radioOne).publishNewVideo();
        radioOne.register(greta);
        ((YTChannel) radioOne).publishNewVideo();
        radioOne.unregister(greta);
        ((YTChannel) radioOne).publishNewVideo();

    }
}
