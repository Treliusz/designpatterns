package observer;

import java.util.ArrayList;

public class YTChannel implements Subject {

    private ArrayList<Observer> observersList;

    public YTChannel() {
        observersList = new ArrayList<Observer>();
    }

    public void publishNewVideo() {
        System.out.println("YTChannel: Publishing new video");
        notifyObservers();
    }

    public void register(Observer o) {
        observersList.add(o);
    }

    public void unregister(Observer o) {
        observersList.remove(o);
    }

    public void notifyObservers() {
        for (Observer o : observersList) {
            o.update();
        }
    }
}
