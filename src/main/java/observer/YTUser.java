package observer;

public class YTUser implements Observer {

    private String name;
    private int videosToWatch;

    public YTUser(String name) {
        this.name = name;
        videosToWatch = 0;
    }
    public void update() {
        videosToWatch++;
        System.out.println("Hej " + name + "! There is a new video" +
                " You have " + videosToWatch + " video to watch");

    }
}
